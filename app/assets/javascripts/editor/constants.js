import { __ } from '~/locale';

export const EDITOR_LITE_INSTANCE_ERROR_NO_EL = __(
  '"el" parameter is required for createInstance()',
);

export const URI_PREFIX = 'gitlab';
export const CONTENT_UPDATE_DEBOUNCE = 250;

export const ERROR_INSTANCE_REQUIRED_FOR_EXTENSION = __(
  'Editor Lite instance is required to set up an extension.',
);
